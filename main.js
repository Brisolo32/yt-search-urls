const yts = require('yt-search')
const prompt = require('prompt-sync')({sigint: true})
const fs = require('node:fs')

let query = prompt('Query: ')
let limit = prompt('Limit: ')

async function getUrls(query, limit) {
    if (limit == '') {
        limit = 1
    } else if (query == '') {
        console.log('\nSearch cannot be empty!')
        process.exit()
    } else if (limit > 100) {
        console.log('\nLimit cannot be greater than 100!')
        process.exit()
    } else if (isNaN(limit)) {
        console.log('\nLimit must be a number!')
        process.exit()
    } else if (limit < 0) {
        console.log('\nLimite must be greater than 0!')
        process.exit()
    }

    const r = await yts(query)
    const videourls = r.videos.slice(0, parseInt(limit))
    let date = new Date()
    let dateWithTime = date.getDate() + '-' + date.getMonth() + '-' + date.getFullYear() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds()

    videourls.forEach( function (v) {
        fs.appendFileSync(`urls-(${dateWithTime}).txt`, v.url + '\n')
    })

    console.log('\nUrls saved in urls-(' + dateWithTime + ').txt')

}

getUrls(query, limit)

//  -[----->+<]>----.---[-->+++<]>.---[----->+
//  <]>-.---------.++++++++++.----.---.+++.>-
//  [----->+<]>.-.